![Just look at it! \label{fig:duck1}](res/img/duck.jpg "fancy duck")

If you just want a regular inline image, just make sure it is not the only thing in the paragraph. One way to do this is to insert a nonbreaking space after the image:

![Just look at it some more!](res/img/duck.jpg "fancy duck")\

Note that the inline image will not appear in the list of figures and cannot be referenced!
