Formulas can be created with common LaTex markup:

\begin{align}
Precision &= \frac{tp}{tp + fp}
&= \frac{10.146}{10.146 + 17} \approx 99,83 \%
\end{align}

\begin{align}
Recall &= \frac{tp}{tp + fn}
&= \frac{10.146}{10.146 + 9} \approx 99,91 \%
\end{align}

\begin{align}
Richtigkeit &= \frac{tp + tn}
{tp + tn + fp + fn}
&= \frac{10.146 + 1.459}{10.146 + 1.459 + 17 + 9} \approx 99,78 \%
\end{align}

Inline formulas are enclosed between two *$* signs:

$E = e_1 \ldots e_p$

$E_{best} = \operatorname{arg\,max}_E P(E|F)$.
