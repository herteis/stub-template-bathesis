Headers can be defined in different styles, see official Pandoc documentation at <http://johnmacfarlane.net/pandoc/README.html#headers>.

It might be a good idea to outsource different blocks of chapters to subdirectories and include them into a common file, as done here.

Note that GPP does not allow includes in files more than one level down the directory tree, starting from the main file. For example if you use an include statement in this very file, you will get a *file not found error*, even if the file exists:

```
< #include ../../res/foo.md>
```

## Lists

See the official Pandoc documentation at <http://johnmacfarlane.net/pandoc/README.html#lists> for more list types.


### Bullet lists

* fruits
    + apples
        - macintosh
        - red delicious
    + pears
    + peaches
* vegetables
    + broccoli
    + chard


### Ordered lists

5. one
7. two
1. three


### Definition lists

Term 1

:   Definition 1

Term 2 with *inline markup*

:   Definition 2

        { some code, part of Definition 2 }

    Third paragraph of definition 2.

Term 1
  ~ Definition 1
Term 2
  ~ Definition 2a
  ~ Definition 2b


## Block quotes

Famous quote:

> Pandoc rocks!


## Footnotes

Here, have a footnote^[this is a footnote]!
