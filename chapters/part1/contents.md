Files/ directories you most likely want to edit:

- `main.md`: main Pandoc file with the general thesis structure (includes the files in `chapters/`)
- `chapters/*`: directory with the included chapters as single files
- `bibliography.bib`: BibTex file
- `res/*`: various resource files, like images

Files you may want to edit:

- `Makefile`: makefile for the compile directives
- `res/gpp-defines.gpp`: makro definition file for LaTex commands, used by the Pandoc Preprocessor (PPP)

Files you should not edit:

- `README.md`: this readme
- `out/*`: directory with the compiled output files (PDF, TEX, HTML)
- `res/csl/*`: BibTex style files
- `*.sty`: LaTex style files (you can tuck them into a subdirectory by setting the `TEXINPUTS` variable in your shell accordingly, or just install them globally)
- `project.json`: project configuration file for *stub* (can be deleted after downloading the template)
