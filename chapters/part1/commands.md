All typesetting commands are implemented via make.

## Basic commands

Pandoc to PDF:


``` { .bash }
make
```

or

``` { .bash }
make pdf
```

Pandoc to HTML does not require Latex and is perfect for quick drafts. Note that certain Latex-specific packages won't work here (like tikz graphs):


``` { .bash }
make html
```

Pandoc to raw TEX if you need it for inspection and debugging:


``` { .bash }
make tex
```

Clean the working directory from all intermediate output files, like the LaTex meta files:

``` { .bash }
make clean
```

Clean the working directory from all output files, including PDF and HTML:

``` { .bash }
make clobber
```

Spelling correction (with `en_US` for English respectively):

``` { .bash }
aspell -H -d de_DE -c main.md
```

## Optional parameters

Make accepts the following parameters:

- `DOC` (=main.md):
- `BIB` (=bibliography.bib)
- `CSS` (=http://netdna.bootstrapcdn.com/bootswatch/3.1.1/yeti/bootstrap.min.css)

Set one or more parameters by changing the Makefile or on the command line:

``` { .bash }
make html DOC=main.md BIB=bibliography.bib CSS=http://netdna.bootstrapcdn.com/bootswatch/3.1.1/darkly/bootstrap.min.css
```

You can find a selection of CSS styles here:

<http://www.bootstrapcdn.com/#bootswatch_tab>
