# Originally based on Daniel's typesetting template Makefile (https://github.com/xdbr/stub-template-typesetting)
# and derived from David's spin of it (URL!)

# BASIC SETUP
# =======================================================================================

# any of these variables can be overwritten from the command line as well

# FILES AND DIRECTORIES
# ----------------------------------------

# The name of the main (skeleton) document
DOCUMENT          ?= main.md

# The name of the cover file
COVER_FILE        ?= titelblatt.tex

# Include external abstract file? Looks for a separate (TEX) file 'abstract' in INCLUDES_PATH
# (default: yes)
EXTERNAL_ABSTRACT ?= no

# Directory of sub-documents (to be used GPP)
INCLUDES_PATH     ?= Abschnitte
INCLUDES_PATH2    ?= Materialien
INCLUDES_PATH3    ?= Materialien/Tabellen

# Directory of resources and working data
RESOURCES_PATH    ?= res

# Directory of graphics
GRAPHICS_PATH     ?= Materialien/Grafiken

# Directory of bibliography and citation styles
BIBLIOGRAPHY_PATH ?= Biblio
# NOTE: By default CSL is assumed as format / style language for bibliographical references
# see 'Makefile.in/general_setup.mk' if you want to change to Natbib / BibTeX.
USE_CSL           ?= yes
CSL_PATH = $(BIBLIOGRAPHY_PATH)/CSL-Stile/
CSL_STYLE = chicago-author-date-de-etal.csl

# Only set this, if you want to use Natbib / BibTeX
NATBIB_STYLE      ?= ./$(RESOURCES_PATH)/mydinat.bst

# Custom output directory
# (default: none)
# OUTPUT_PATH       ?=

# The name of the LaTeX preamble file(not used by default, see pandoc configs)
# PREAMBLE_FILE     ?= vorspann.tex

# OPTIONS AND CONFIGURATIONS
# ----------------------------------------

# Directory of configuration files
CONFIG_PATH       ?= .configs

# Path of modules (Pandoc filters, preprocessors, etc.)
MODULE_PATH       ?= .modules

# For the main setup of Pandoc and LaTeX output, have a look at
# .configs/pandoc_cfg and Makefile.in/pandoc_flags.mk

# Improvments for code listings in LaTeX
LST_TWEAKS = $(RESOURCES_PATH)/listings-tweaks.tex

# load individual settings from config files
include Makefile.in/general_setup.mk
include Makefile.in/gpp_flags.mk
include Makefile.in/pandoc_flags.mk
include Makefile.in/recipes.mk
include Makefile.in/graphics.mk
