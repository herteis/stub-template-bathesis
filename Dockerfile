from ubuntu:14.04
maintainer WAST Group <wast-list@lists.ifi.lmu.de>

run apt-get -y update
run apt-get -y upgrade
run apt-get -y install pandoc pandoc-citeproc

# See: http://packages.ubuntu.com/trusty/texlive-full
run apt-get -y install texlive-full

run apt-get -y install build-essential unzip cpanminus time

# Important: Use headless option - you will thank me later ;)
run apt-get -y install openjdk-7-jre-headless
run apt-get -y install imagemagick
run apt-get -y install librsvg2-bin librsvg2-dev graphviz graphviz-dev
run apt-get -y install gpp

run cpanm Try::Tiny
run cpanm RDF::Trine::Parser
# One testcase is not working, so force installing it!
run cpanm --force RDF::Trine::Exporter::GraphViz

# A few modifications (original manual from David)
# https://gitlab.cis.uni-muenchen.de/David/stub-template-bathesis/blob/master/README.md

# We don't want to use git here...
run cd /opt && curl -LO https://github.com/typesetters/p5-App-pandoc-preprocess/archive/master.zip
run unzip /opt/master.zip -d /opt && rm -f /opt/master.zip
run ln -s /opt/p5-App-pandoc-preprocess-master/bin/ppp /bin/ppp

run cd /opt && curl -LO http://skylink.dl.sourceforge.net/project/ditaa/ditaa/0.9/ditaa0_9.zip
run unzip /opt/ditaa0_9.zip -d /opt/ditaa && rm -f /opt/ditaa0_9.zip
# Notice don't use echos -e option here (Dockerfile specific!)
run echo '#!/bin/bash\nexec java -Dfile.encoding=UTF-8 -jar /opt/ditaa/ditaa0_9.jar "$@"' > /bin/ditaa && chmod a+x /bin/ditaa

run mkdir -p /opt/plantuml && cd /opt/plantuml && curl -LO http://optimate.dl.sourceforge.net/project/plantuml/plantuml.jar
run echo '#!/bin/bash\nexec java -Djava.awt.headless=true -Dfile.encoding=UTF-8 -jar /opt/plantuml/plantuml.jar "$@"' > /bin/plantuml && chmod a+x /bin/plantuml

run cd /opt && curl -LO https://github.com/wandernauta/yuml/archive/master.zip
run unzip /opt/master.zip -d /opt && rm -f /opt/master.zip
run ln -s /opt/yuml-master/yuml /bin/yuml
