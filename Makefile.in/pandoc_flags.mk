#----------------------------#
# Pandoc flags and variables #
#----------------------------#

define PDC_EXTENSIONS
 autolink_bare_uris\
 escaped_line_breaks\
 example_lists\
 fancy_lists\
 fenced_code_attributes\
 fenced_code_blocks\
 header_attributes\
 implicit_figures\
 implicit_header_references\
 lists_without_preceding_blankline\
 multiline_tables\
 pipe_tables\
 raw_tex\
 shortcut_reference_links\
 subscript\
 superscript\
 tex_math_dollars\
 yaml_metadata_block
endef

define PDC_FLAGS
 --smart \
 --standalone \
 --latex-engine=$(LATEX_ENG) \
 --listings \
 --include-before-body=$(LST_TWEAKS)\
 --include-before-body=$(COVER) \
 $(BIBFILES) \
 $(BIBLIO_PARAMETER) \
 -f markdown$(subst $(SPACE),+,$(PDC_EXTENSIONS)) \
 -o $(DOCUMENTNAME)$@
endef

define FILTER_FLAGS
 --filter $(MODULE_PATH)/pandoc-crossref \
 -M "crossrefYaml=$(CROSSREF_CONFIG)" \
 --filter pandoc-citeproc
endef

# TODO: make this conditional and keep in define block
# --include-before-body=$(INCLUDES_PATH)/abstract \

# TO BE USED FOR MINTED

# --no-highlight \
# --filter $(MODULE_PATH)/minted.py \

# DISABLED PANDOC SETTINGS

# --include-in-header=$(PREAMBLE) \
# -V documentclass="$(DOCTYPE)" \
# -V classoption="abstracton" \
# -V classoption="a4paper" \
# -V mainfont=$(FONT) \
# -V fontfamilyoptions=$(FONTOPTIONS) \
# -V fontsize=$(FONTSIZE) \
# -V linkcolor=black \
# -V lang=$(LANGUAGE) \
# --toc \
# --toc-depth=$(TOCDEPTH) \
# --number-sections \
# -V include-before=$(PRE_PAGENO_STYLE) \

# --natbib \
# -V documentclass=book \
# -V include-after='\deftranslation[to=German]{Acronyms}{Abkürzungsverzeichnis}\printglossary[type=\acronymtype,style=long]' \
