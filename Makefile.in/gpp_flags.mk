# ----------------------------#
# GPP flags                   #
# ----------------------------#

define GPP_FLAGS
 -I $(INCLUDES_PATH) \
 -I $(INCLUDES_PATH2) \
 -I $(INCLUDES_PATH3) \
 -U "<#" ">" "\B" "|" ">" "<" ">" "#" "" \
 -DLATEX \
 -DPAGEREFS \
 -DMINITOC \
 -DMAKEINDEX \
 -DSIDENOTES \
 -DPARTS \
 -DABSTRACT \
 -DTOC
endef

# disabled flags:
# -x
