INKSCAPE_CMD ?= inkscape
INKSCAPE_FLAGS ?= -D -z -d=300

SVG_SOURCES = $(wildcard $(GRAPHICS_PATH)/*)
PDFTEX_GRAPHICS = $(patsubst %.svg,%.pdf,$(SVG_SOURCES))

graphics: $(PDFTEX_GRAPHICS)

%.pdf: %.svg
	@$(INKSCAPE_CMD) $(INKSCAPE_FLAGS) --file $< -A $@
