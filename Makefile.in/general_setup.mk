# GENERAL SETTINGS
# ================================================================

# Document supplements
# ---------------------------------------

PREAMBLE ?= $(RESOURCES_PATH)/$(PREAMBLE_FILE)
COVER ?= $(RESOURCES_PATH)/$(COVER_FILE)

# Typesetting for the document itself
# ---------------------------------------

# LaTeX-Engine
LATEX_ENG ?= pdflatex

# Multiple pages per sheet settings
NUP ?= 2x1


# Biblography & Citation style
# ---------------------------------------

# Use custom citation styles (CSL and pandoc-citeproc)?
# (This is the default)
ifeq ($(USE_CSL),yes)
    BIBLIO_PARAMETER=--csl $(CSL_PATH)$(CSL_STYLE)
    BIBLIO_FILES = $(wildcard ./$(BIBLIOGRAPHY_PATH)/*.json)
    BIBLIO_FILETYPE = *.json
else
    # BibTeX (Natbib) will be used otherwise
    BIBLIO_STYLE = $(patsubst %.bst,%,$(NATBIB_STYLE))
    BIBLIO_PARAMETER=--natbib -V biblio-style="$(BIBLIO_STYLE)"
    BIBLIO_FILETYPE = *.bib
endif

BIBLIO_FILES ?= $(wildcard ./$(BIBLIOGRAPHY_PATH)/${BIBLIO_FILETYPE})
BIBFILES ?= $(foreach bibfile,$(BIBLIO_FILES),--bibliography $(bibfile))

# Toolchain options
# ---------------------------------------

# little hack to have variable SPACE contain a whitespace
EMPTY :=
SPACE := $(EMPTY) $(EMPTY)

# pandoc config (handles most of document settings)
PANDOC_CONFIG     ?= $(CONFIG_PATH)/pandoc-cfg.yaml

# Crossref filter settings
CROSSREF_CONFIG   ?= $(CONFIG_PATH)/crossref-cfg.yaml
