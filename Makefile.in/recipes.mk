# =======================================
#           Make recipes
# =======================================

DOCUMENTNAME = $(patsubst %.md,%,$(DOCUMENT))


INFO_MSG="\\n------\\n$$(date +"%H:%M:%S"): Starting typesetting process for '$@'..."
FINISHED_MSG="$$(date +"%H:%M:%S"): Done.\\n"


default: pdf
all: pdf tex mobi html book eco epub docx odt graphics


check:
	@echo "\\nChecking availability of required programs:"
	@echo "----------------------------------------------"
	@sh -c 'for i in pandoc ppp ditaa yuml rdfdot gpp perl dot neato inkscape; do \
		echo $$i... $$( \
		which $$i 2>&1 >/dev/null && echo "\t good."); \
	done'

clean:
	find ./$(OUTPUT_PATH) -type f -iname $(DOCUMENTNAME).\* | grep -v -P bib\|update\|checks\|build\|md\|html\|pdf | xargs rm -rf

clobber:
	find ./$(OUTPUT_PATH) -type f -iname $(DOCUMENTNAME).\* | grep -v -P bib\|update\|checks\|build\|md | xargs rm -rf

pdf: $(DOCUMENTNAME).pdf
tex: $(DOCUMENTNAME).tex

%.pdf %.tex %.mobi %.epub %.odt %.docx : %.md check graphics
	@echo $(INFO_MSG)
	@gpp $(GPP_FLAGS)\ < $< | ppp | pandoc $(PDC_FLAGS) $(FILTER_FLAGS) $(PANDOC_CONFIG) -
	@echo $(FINISHED_MSG)

ppp: check $(DOCUMENT)
	@echo "\\n\\n$$(date +"%H:%M:%S"): Starting typesetting process up to ppp stage..."
	gpp $(GPP_FLAGS)\ < $(DOCUMENT) | ppp
	@echo $(FINISHED_MSG)

book: pdf
	pdfbook $(OUTPUT_PATH)/$(DOCUMENTNAME).pdf --outfile $(OUTPUT_PATH)/$(DOCUMENTNAME)-book.pdf

eco: pdf
	pdfjam --nup $(NUP) --a4paper --landscape $(OUTPUT_PATH)/$(DOCUMENTNAME).pdf --outfile $(OUTPUT_PATH)/$(DOCUMENTNAME)-$(NUP)-eco.pdf

html: check
	@echo "\\n\\nStarting typesetting process to html..."
	gpp $(GPP_FLAGS)\ < $(DOCUMENT) | ppp | pandoc $(PDC_FLAGS)
	@perl -i -pe 's{ppp\-.*?\-.*?/}{}g' $(OUTPUT_PATH)/$(DOCUMENTNAME).html
	@echo "$$(date +"%H:%M:%S"): Done.\\n"

$(BIB):
	perl -i -pe 's{\|}{\/}g' $(BIB)
